//
//  VideoCell.swift
//  funnyvideo
//
//  Created by Kien Nguyen on 5/11/16.
//  Copyright © 2016 Kien Nguyen. All rights reserved.
//

import Foundation
import UIKit

class VideoCell: UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let colorView = UIView()
        colorView.backgroundColor = UIColor(red: 242/255, green: 242/255, blue: 242/255, alpha: 1)
        self.selectedBackgroundView = colorView
    }


}