//
//  VideoDetailController.swift
//  funnyvideo
//
//  Created by Kien Nguyen on 5/13/16.
//  Copyright © 2016 Kien Nguyen. All rights reserved.
//

import Foundation
import AVKit
import AVFoundation
import UIKit
import YoutubeSourceParserKit


class VideoDetailController: UIViewController{
    var playerItem:AVPlayerItem?
    var player:AVPlayer?

    @IBOutlet weak var videoPlayerFrame: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        //initScreen()
        loadVideo()
    }
    
    func initScreen(){
        // self.navigationItem.title = eventName
        let navBar: UINavigationBar! = navigationController?.navigationBar
        navBar.shadowImage = UIImage()
        //navBar.translucent = true
        navBar.backgroundColor = UIColor.clearColor()
        self.navigationController?.view.backgroundColor = UIColor.clearColor()
        navBar.barStyle = .BlackTranslucent
        navBar.setBackgroundImage(imageLayerForGradientBackground(), forBarMetrics: UIBarMetrics.Default)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.barStyle = .Black
        navigationController?.navigationBar.setBackgroundImage(nil, forBarMetrics: UIBarMetrics.Default)
    }
    
    func loadVideo() {
        
        let testURL = NSURL(string: "https://www.youtube.com/watch?v=VioTZBzwRIQ")!
        Youtube.h264videosWithYoutubeURL(testURL) { (videoInfo, error) -> Void in
            if let videoURLString = videoInfo?["url"] as? String,
                videoTitle = videoInfo?["title"] as? String {
                    print("\(videoTitle)")
                    print("\(videoURLString)")
                    
                    let videoURL = NSURL(string: videoURLString)
                    let player = AVPlayer(URL: videoURL!)
                    let pvc = AVPlayerViewController()
                    pvc.showsPlaybackControls = false
                    pvc.player = player
                    pvc.view.frame = self.videoPlayerFrame.frame
                    self.addChildViewController(pvc)
                    self.view.addSubview(pvc.view)
                    pvc.player!.play()
            }
        }
    }

    private func imageLayerForGradientBackground() -> UIImage {
        var updatedFrame = self.navigationController?.navigationBar.bounds
        updatedFrame!.size.height += 20
        var layer = CAGradientLayer.gradientLayerForBounds(updatedFrame!, fromColor: UIColor(red: 0, green: 0,blue: 0, alpha: 1), toColor: UIColor(red: 0, green: 0, blue: 0, alpha: 0))
        UIGraphicsBeginImageContext(layer.bounds.size)
        layer.renderInContext(UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }

    
    
}