//
//  ViewController.swift
//  funnyvideo
//
//  Created by Kien Nguyen on 4/28/16.
//  Copyright © 2016 Kien Nguyen. All rights reserved.
//

import UIKit
import PageMenu
import AMScrollingNavbar

class ViewController: UIViewController, CAPSPageMenuDelegate, ScrollingNavigationControllerDelegate {

    var pageMenu : CAPSPageMenu?
    var controllerArray : [UIViewController] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        setupLogo()
        setupViews()
        self.navigationController?.navigationBar.barStyle = UIBarStyle.BlackTranslucent;
    }
    /*
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if let navigationController = self.navigationController as? ScrollingNavigationController {
            print("viewWillAppear")
            navigationController.scrollingNavbarDelegate = self
        }
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        print("viewDidDisappear")

        if let navigationController = self.navigationController as? ScrollingNavigationController {
            navigationController.stopFollowingScrollView()
        }
    }

    override func scrollViewShouldScrollToTop(scrollView: UIScrollView) -> Bool {
        if let navigationController = self.navigationController as? ScrollingNavigationController {
            print("scrollViewShouldScrollToTop")
            navigationController.showNavbar(animated: true)
        }
        return true
    }
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        if let navigationController = self.navigationController as? ScrollingNavigationController {
            print("viewWillDisappear")
            navigationController.showNavbar(animated: true)
        }
    }

    
    func scrollingNavigationController(controller: ScrollingNavigationController, didChangeState state: NavigationBarState) {
        switch state {
        case .Collapsed:
            print("navbar collapsed")
        case .Expanded:
            print("navbar expanded")
        case .Scrolling:
            print("navbar is moving")
        }
    }
    */
    
    func setupLogo(){
        navigationItem.title = "Sm;)e"
        /*
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 94, height: 18))
        imageView.contentMode = .ScaleAspectFit
        let image = UIImage(named: "smile_logo")
        imageView.image = image
        navigationItem.titleView = imageView
        */
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

extension ViewController {
    
    func setupViews(){
        var controllerArray : [UIViewController] = []
        var controller = storyboard?.instantiateViewControllerWithIdentifier("videoListController") as! VideoListController
        controller.title = "MỚI NHẤT"
        controller.parentNavigationController = self.navigationController
        controllerArray.append(controller)
        
        controller = storyboard?.instantiateViewControllerWithIdentifier("videoListController") as! VideoListController
        controller.title = "HOT NHẤT"
        controller.parentNavigationController = self.navigationController
        controllerArray.append(controller)
        
        controller = storyboard?.instantiateViewControllerWithIdentifier("videoListController") as! VideoListController
        controller.title = "LỊCH SỬ"
        controller.parentNavigationController = self.navigationController
        controllerArray.append(controller)
        
        let pageMenuOption :[CAPSPageMenuOption] = [
            .MenuItemSeparatorWidth(0),
            .EnableHorizontalBounce(false),
            .MenuHeight(36.0),
            .MenuMargin(0),
            .SelectionIndicatorHeight(3.0),
            .MenuItemFont(UIFont(name: "HelveticaNeue-Medium", size: 14.0)!),
            .BottomMenuHairlineColor(UIColor(red: 20.0/255.0, green: 20.0/255.0, blue: 20.0/255.0, alpha: 0.1)),
            .SelectionIndicatorColor(UIColor(red: 255/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0)),
            .ViewBackgroundColor(UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)),
            .ScrollMenuBackgroundColor(UIColor(red: 255/255, green:255/255, blue: 255/255, alpha: 1)),
            .UnselectedMenuItemLabelColor(UIColor(red: 41 / 255.0, green: 46 / 255.0, blue: 65 / 255.0, alpha: 1.0)),
            .SelectedMenuItemLabelColor(UIColor(red: 255/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0)),
        ]
        
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRectMake(0.0, 0.0, self.view.frame.width, self.view.frame.height), pageMenuOptions: pageMenuOption)
        pageMenu!.delegate = self
        self.view.addSubview(pageMenu!.view)
    }
}

extension ViewController {
    
    func willMoveToPage(controller: UIViewController, index: Int) {
    }
    
    func didMoveToPage(controller: UIViewController, index: Int) {
    }

}

