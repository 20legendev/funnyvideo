//
//  BigPictureVideoCell.swift
//  funnyvideo
//
//  Created by Kien Nguyen on 5/9/16.
//  Copyright © 2016 Kien Nguyen. All rights reserved.
//

import Foundation
import UIKit

class BigPictureVideoCell: VideoCell {
    
    @IBOutlet weak var mainPicture: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}