//
//  LatestVideoViewController.swift
//  funnyvideo
//
//  Created by Kien Nguyen on 5/3/16.
//  Copyright © 2016 Kien Nguyen. All rights reserved.
//

import Foundation
import UIKit
import AMScrollingNavbar
import AVKit
import AVFoundation
import YoutubeSourceParserKit

class VideoListController: UITableViewController, ScrollingNavigationControllerDelegate{
    
    var parentNavigationController : UINavigationController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()

    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
    /*
    override func scrollViewShouldScrollToTop(scrollView: UIScrollView) -> Bool {
        if let navigationController = self.navigationController as? ScrollingNavigationController {
            navigationController.showNavbar(animated: true)
        }
        return true
    }

    
    func scrollingNavigationController(controller: ScrollingNavigationController, didChangeState state: NavigationBarState) {
        switch state {
        case .Collapsed:
            print("navbar collapsed")
        case .Expanded:
            print("navbar expanded")
        case .Scrolling:
            print("navbar is moving")
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        if let navigationController = self.parentNavigationController as? ScrollingNavigationController {
            navigationController.showNavbar(animated: true)
        }
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        
        if let navigationController = self.parentNavigationController as? ScrollingNavigationController {
            navigationController.stopFollowingScrollView()
        }
    }



        
     */
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCellWithIdentifier("BigPictureVideoCell") as! BigPictureVideoCell
            return cell
        }else{
            let cell = tableView.dequeueReusableCellWithIdentifier("VideoViewCell") as! VideoViewCell
            return cell
        }

    }
        
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let newVC = self.storyboard?.instantiateViewControllerWithIdentifier("VideoDetailController") as? VideoDetailController
        parentNavigationController!.pushViewController(newVC!, animated: true)
        // loadVideo()

    }
    
    func loadVideo() {
        
        let testURL = NSURL(string: "https://www.youtube.com/watch?v=swZJwZeMesk")!
        Youtube.h264videosWithYoutubeURL(testURL) { (videoInfo, error) -> Void in
            if let videoURLString = videoInfo?["url"] as? String,
                videoTitle = videoInfo?["title"] as? String {
                    print("\(videoTitle)")
                    print("\(videoURLString)")
                    
                    let videoURL = NSURL(string: videoURLString)
                    let player = AVPlayer(URL: videoURL!)
                    let playerViewController = AVPlayerViewController()
                    playerViewController.player = player
                    playerViewController.view.frame = CGRectMake(0, 0, 300, 50)
                    self.presentViewController(playerViewController, animated: true) {
                        
                        playerViewController.player!.play()
                        
                    }
                    
            }
        }
    }
    

}

extension VideoListController{
    func configureTableView() {
        self.tableView.estimatedRowHeight = 80
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.setNeedsLayout()
        self.tableView.layoutIfNeeded()
    }

}